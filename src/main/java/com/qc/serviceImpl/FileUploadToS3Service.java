package com.qc.serviceImpl;

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadToS3Service {

	String uploadFileToS3(String key, String filePath);

	String uploadMultiPartFileToS3(String key, MultipartFile multipartFile);

}
