package com.qc.config;
import com.amazonaws.ClientConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;

/**
 * Created by Tahseem on 02/02/2018.
 */

@Configuration
public class AWSConfiguration {
	


   @Value("${cloud.aws.credentials.accessKey}")
   public String accessKey;

   @Value("${cloud.aws.credentials.secretKey}")
   public String secretKey;

   @Value("${cloud.aws.region.static}")
   public String region;


    @Bean
    public BasicAWSCredentials basicAWSCredentials() {
        return new BasicAWSCredentials(accessKey, secretKey);
    }


    @Bean
    public ClientConfiguration clientConfiguration(){
    	ClientConfiguration clientConfiguration=new ClientConfiguration();
    	clientConfiguration.setMaxConnections(5000);
    	clientConfiguration.setMaxErrorRetry(1);
    	return clientConfiguration;
    }
    @Bean
    public AmazonS3 amazonS3Client(AWSCredentials awsCredentials ,ClientConfiguration clientConfiguration) {
    	return AmazonS3ClientBuilder.standard()
    			.withRegion(Regions.fromName(region))
    			.withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
    			.withClientConfiguration(clientConfiguration)
    			.build();
    }

    @Bean
    TransferManager transferManager(AmazonS3 amazonS3Client){
    	return TransferManagerBuilder.standard()
    			.withMultipartUploadThreshold(Long.valueOf(5*1024*1024))
    			.withMinimumUploadPartSize(Long.valueOf(5*1024*1024))
    			.withS3Client(amazonS3Client)
    			.build();
    }
	
}

