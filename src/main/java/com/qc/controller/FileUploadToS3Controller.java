package com.qc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.qc.serviceImpl.FileUploadToS3Service;

@RestController
public class FileUploadToS3Controller {

	@Autowired
	FileUploadToS3Service fileUploadToS3Service;
	
	@RequestMapping("/api")
	public String api(){
		return "hell";
	}
	
	@RequestMapping(value="/upload",method=RequestMethod.POST)
	public String uploadFile(){
		String keyPath = fileUploadToS3Service.uploadFileToS3("TEST/2018/02/02/test.txt","D:\\test.txt");
		return keyPath;
	}
	
	
}
