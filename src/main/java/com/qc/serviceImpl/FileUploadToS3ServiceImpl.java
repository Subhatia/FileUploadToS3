package com.qc.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.model.UploadResult;

/**
 * Created by tahseem on 02/02/2018.
 */
@Service
public class FileUploadToS3ServiceImpl implements FileUploadToS3Service {


    @Autowired
    TransferManager transferManager;

    @Value("${cloud.aws.s3.bucket}")
    private String bucketName;

    @Value("${cloud.aws.s3.url}")
    private String s3UrlPrefix;

    @Override
    public String uploadFileToS3(String key, String filePath) {
        final UploadResult[] uploadResult = {null};
        File file =new File(filePath);
        PutObjectRequest putObjectRequest=new PutObjectRequest(bucketName,key, file);
        Upload upload = transferManager.upload(putObjectRequest);
        try {
            upload.waitForCompletion();
            uploadResult[0] =upload.waitForUploadResult();
            return uploadResult[0].getKey();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return e.getMessage();
        }


    }

    @Override
    public String uploadMultiPartFileToS3(String key, MultipartFile multipartFile){
        final UploadResult[] uploadResult = {null};
        try {
            InputStream inputStream=multipartFile.getInputStream();
            PutObjectRequest putObjectRequest=new PutObjectRequest(bucketName,key,inputStream,new ObjectMetadata());
            Upload upload=transferManager.upload(putObjectRequest);
            upload.waitForCompletion();
            uploadResult[0] =  upload.waitForUploadResult();
            if(uploadResult[0].getETag() != null){
                return "s3UrlPrefix"+"/"+bucketName+"/"+uploadResult[0].getKey();
            }else{
            	return null;
            }
          
        } catch (IOException  |InterruptedException e) {
            e.printStackTrace();
            return null;
        }



    }

}
